// angular
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// rxjs
import { map, catchError, tap } from 'rxjs/operators';
import { of, Observable } from 'rxjs';

// models
import { Port } from '../modules/shipping-planner/models/port';
import { Filter } from '../modules/shipping-planner/models/filter';
import { MapBounds } from '../modules/shipping-planner/models/map-bounds';

const API_URL = 'http://localhost:3000/api';
const END_POINTS = {
  ports: '/ports',
  portDetails: '/port'
};

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private httpClient: HttpClient) {}

  public getHarbors(payload: MapBounds): Observable<Port[]> {
    const { maxlat, minlat, minlon, maxlon } = payload;
    const params: any = {
      maxlat: maxlat.toString(),
      minlat: minlat.toString(),
      minlon: minlon.toString(),
      maxlon: maxlon.toString()
    };

    const url = `${API_URL}${END_POINTS['ports']}`;

    return this.httpClient
      .get<Port[]>(url, { params })
      .pipe(
        map((data: any) => data.ports),
        catchError(err => of(err))
      );
  }

  public getPortDetails(payload: number): Observable<Port> {
    const params: any = {
      id: payload
    };

    const url = `${API_URL}${END_POINTS['portDetails']}`;

    return this.httpClient
      .get<Port>(url, { params })
      .pipe(
        map((data: any) => data.ports),
        catchError(err => of(err))
      );
  }
}
