import { AppComponent } from './app.component';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

export const appRoutes: Routes = [
  { path: '', component: HomeComponent, data: { title: 'Home ' } },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  {
    path: 'counter',
    loadChildren: 'src/app/modules/counter/counter.module#CounterModule'
  },
  {
    path: 'shipping-planner',
    loadChildren: 'src/app/modules/shipping-planner/shipping-planner.module#ShippingPlannerModule'
  },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [CommonModule, RouterModule.forRoot(appRoutes)],
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    HomeComponent
  ]
})
export class AppRoutingModule {}
