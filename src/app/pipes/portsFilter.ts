import { Pipe, PipeTransform } from '@angular/core';
import { Port } from '../modules/shipping-planner/models/port';
import { Filter } from '../modules/shipping-planner/models/filter';

@Pipe({
    name: 'portsFilter',
    pure: false
})
export class PortsFilterPipe implements PipeTransform {
    transform(items: Array<Port>, filter: Filter): any {
        if (!items || !filter) {
            return items;
        }
        // filter items array, items which match and return true will be
        // kept, false will be filtered out
        return items.filter(item => this.filterPort(item, filter));
    }

    filterPort (port: Port, filter: Filter) {
        if (filter.isCargo && filter.isCruise) {
            return true;
        } else if (filter.isCargo) {
            return port.type === 'port';
        } else if (filter.isCruise) {
            return port.type === 'cruise';
        } else {
            return false;
        }
    }
}
