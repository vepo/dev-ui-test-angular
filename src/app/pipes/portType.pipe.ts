import { Pipe, PipeTransform } from '@angular/core';
import { Port } from '../modules/shipping-planner/models/port';

@Pipe({ name: 'portType' })
export class PortTypePipe implements PipeTransform {
  transform(value: Port): string {
    if (value) {
      switch (value.type) {
        case 'port':
          return 'Cargo';
        case 'cruise':
          return 'Cruise';
        default:
          return value.type;
      }
    }
    return '';
  }
}
