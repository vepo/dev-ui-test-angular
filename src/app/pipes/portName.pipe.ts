import { Pipe, PipeTransform } from '@angular/core';
import { Port } from '../modules/shipping-planner/models/port';

@Pipe({ name: 'portName' })
export class PortNamePipe implements PipeTransform {
  transform(value: Port): string {
      if (value) {
        return value.name ? value.name : `N/A (${value.city})`;
      }
      return '';
  }
}
