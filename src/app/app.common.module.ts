import { NgModule } from '@angular/core';
import { PortNamePipe } from './pipes/portName.pipe';
import { PortTypePipe } from './pipes/portType.pipe';
import { PortsFilterPipe } from './pipes/portsFilter';

@NgModule({
    imports: [
    ],
    declarations: [PortNamePipe, PortTypePipe, PortsFilterPipe],
    exports: [PortNamePipe, PortTypePipe, PortsFilterPipe]
})
export class AppCommonModule {
}
