import { ShippingPlannerModule } from './shipping-planner.module';

describe('ShippingPlannerModule', () => {
  let shippingPlannerModule: ShippingPlannerModule;

  beforeEach(() => {
    shippingPlannerModule = new ShippingPlannerModule();
  });

  it('should create an instance', () => {
    expect(shippingPlannerModule).toBeTruthy();
  });
});
