// angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// app imports
import { PortListComponent } from './port-list.component';
import { FiltersModule } from './filters/filters.module';
import { PortListItemModule } from './port-list-item/port-list-item.module';
import { AppCommonModule } from '../../../../../app.common.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    AppCommonModule,
    FiltersModule,
    PortListItemModule,
  ],
  declarations: [PortListComponent],
  exports: [PortListComponent]
})
export class PortListModule { }
