import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FiltersComponent } from './filters.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from '../../../../../../reducers';
import { EffectsModule } from '@ngrx/effects';
import { SHIPPING_PLANNER_STORE_KEY, ShippingPlannerReducer } from '../../../../store/shipping-planner.reducer';
import { ShippingPlannerEffects } from '../../../../store/shipping-planner.effects';
import { RouterTestingModule } from '@angular/router/testing';

describe('FiltersComponent', () => {
  let component: FiltersComponent;
  let fixture: ComponentFixture<FiltersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FontAwesomeModule,
        ReactiveFormsModule,
        HttpClientModule,
        RouterTestingModule,
        // ngrx store initialization
        StoreRouterConnectingModule.forRoot({ stateKey: 'router' }),
        StoreModule.forRoot(reducers, { metaReducers }),
        EffectsModule.forRoot([]),

        StoreModule.forFeature(SHIPPING_PLANNER_STORE_KEY, ShippingPlannerReducer),
        EffectsModule.forFeature([ShippingPlannerEffects])
      ],
      declarations: [ FiltersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
