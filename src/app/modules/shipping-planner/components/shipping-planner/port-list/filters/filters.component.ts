// angular
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
// rxjs
import { tap, takeUntil } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
// app imports
import { ShippingPlannerService } from '../../../../store/shipping-planner.service';
import { Filter } from '../../../../models/filter';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit, OnDestroy {
  private unsubscribe$: Subject<void> = new Subject<void>();
  filters$: Observable<Filter>;
  form: FormGroup;

  constructor(private formBuilder: FormBuilder, private shippingPlannerService: ShippingPlannerService) {
    this.filters$ = this.shippingPlannerService.filters$;
    this.filters$.pipe(
      tap(filter => this.form.patchValue(filter))
    );
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      isCruise: [true],
      isCargo: [true],
    });

    this.onChanges();
  }

  onChanges(): void {
    this.form.valueChanges
      .pipe(
        takeUntil(this.unsubscribe$)
      )
      .subscribe(form => {
        this.shippingPlannerService.updatePortTypeFilter(form);
      });
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
