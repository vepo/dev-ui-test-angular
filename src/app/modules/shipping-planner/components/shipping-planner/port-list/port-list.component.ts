// angular
import { Component, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

// rxjs
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

// models
import { Port } from '../../../models/port';
// app imports
import { ShippingPlannerService } from '../../../store/shipping-planner.service';
import { Filter } from '../../../models/filter';

@Component({
  selector: 'app-port-list',
  templateUrl: './port-list.component.html',
  styleUrls: ['./port-list.component.scss']
})
export class PortListComponent {
  @Input() viewMode = 'map';

  private unsubscribe$: Subject<void> = new Subject<void>();

  portList: Array<Port> = [];
  loadingPortList$: Observable<boolean>;
  selectedPort: Port = undefined;
  filters: Filter = new Filter(true, true);

  constructor(private shippingPlannerService: ShippingPlannerService, private router: Router) {
    this.shippingPlannerService.selectedPort$
      .pipe(
        takeUntil(this.unsubscribe$)
      )
      .subscribe(port => {
        // set the selected port id
        this.selectedPort = port;
      });

    this.shippingPlannerService.filters$
      .pipe(
        takeUntil(this.unsubscribe$)
      )
      .subscribe(filters => {
        this.filters = filters;
      });

      this.shippingPlannerService.portList$
      .pipe(
        takeUntil(this.unsubscribe$)
      )
      .subscribe(ports => {
        this.portList = ports;
      });
  }

  onSelect(port: Port) {
    // set the current selected port and route to port details page
    this.selectedPort = port;
    this.shippingPlannerService.selectPort(port);
    if (this.viewMode === 'details') {
      // route to port details page
      this.router.navigate(['shipping-planner/details', port.id]);
    }
  }
}
