import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PortListComponent } from './port-list.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { PortListItemModule } from './port-list-item/port-list-item.module';
import { FiltersModule } from './filters/filters.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { SHIPPING_PLANNER_STORE_KEY, ShippingPlannerReducer } from '../../../store/shipping-planner.reducer';
import { ShippingPlannerEffects } from '../../../store/shipping-planner.effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { reducers, metaReducers } from '../../../../../reducers';
import { HttpClientModule } from '@angular/common/http';
import { Filter } from '../../../models/filter';
import { AppCommonModule } from '../../../../../app.common.module';

describe('PortListComponent', () => {
  let component: PortListComponent;
  let fixture: ComponentFixture<PortListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ RouterTestingModule, FontAwesomeModule,
        FiltersModule,
        PortListItemModule,
        HttpClientModule,
        AppCommonModule,
        // ngrx store initialization
        StoreRouterConnectingModule.forRoot({ stateKey: 'router' }),
        StoreModule.forRoot(reducers, { metaReducers }),
        EffectsModule.forRoot([]),

        StoreModule.forFeature(SHIPPING_PLANNER_STORE_KEY, ShippingPlannerReducer),
        EffectsModule.forFeature([ShippingPlannerEffects])],
      declarations: [ PortListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortListComponent);
    component = fixture.componentInstance;
    component.portList = [];
    component.filters = new Filter(true, true);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
