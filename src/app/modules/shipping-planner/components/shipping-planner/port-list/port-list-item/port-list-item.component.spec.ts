import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PortListItemComponent } from './port-list-item.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SHIPPING_PLANNER_STORE_KEY, ShippingPlannerReducer } from '../../../../store/shipping-planner.reducer';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { ShippingPlannerEffects } from '../../../../store/shipping-planner.effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { reducers, metaReducers } from '../../../../../../reducers';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { Port } from '../../../../models/port';
import { AppCommonModule } from '../../../../../../app.common.module';

describe('PortListItemComponent', () => {
  let component: PortListItemComponent;
  let fixture: ComponentFixture<PortListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FontAwesomeModule,
        RouterTestingModule,
        HttpClientModule,
        AppCommonModule,
        // ngrx store initialization
        EffectsModule.forRoot([]),
        StoreRouterConnectingModule.forRoot({ stateKey: 'router' }),
        StoreModule.forRoot(reducers, { metaReducers }),

        StoreModule.forFeature(SHIPPING_PLANNER_STORE_KEY, ShippingPlannerReducer),
        EffectsModule.forFeature([ShippingPlannerEffects]),
      ],
      declarations: [ PortListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortListItemComponent);
    component = fixture.componentInstance;
    component.port = new Port(1, 'Port Name', 1, 1, 'Port City', '', 1, 'cruise');
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
