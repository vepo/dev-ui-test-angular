// angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// app imports
import { PortListItemComponent } from './port-list-item.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AppCommonModule } from '../../../../../../app.common.module';

@NgModule({
  imports: [
    CommonModule,
    AppCommonModule,
    // font awesome icons
    FontAwesomeModule
  ],
  declarations: [PortListItemComponent],
  exports: [PortListItemComponent]
})
export class PortListItemModule { }
