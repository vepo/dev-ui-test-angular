import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

import { Port } from '../../../../models/port';

@Component({
  selector: 'app-port-list-item',
  templateUrl: './port-list-item.component.html',
  styleUrls: ['./port-list-item.component.scss']
})
export class PortListItemComponent {
  @Input() viewMode: string;
  @Input() port: Port;
  constructor(private router: Router) { }

  switchView() {
    console.log(this.viewMode);
    // route to port details/map page
    if (this.viewMode === 'details') {
      this.router.navigate(['shipping-planner']);
    } else {
      this.router.navigate(['shipping-planner/details', this.port.id]);
    }
  }
}
