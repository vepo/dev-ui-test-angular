import { PortListItemModule } from './port-list-item.module';

describe('PortListItemModule', () => {
  let portListItemModule: PortListItemModule;

  beforeEach(() => {
    portListItemModule = new PortListItemModule();
  });

  it('should create an instance', () => {
    expect(portListItemModule).toBeTruthy();
  });
});
