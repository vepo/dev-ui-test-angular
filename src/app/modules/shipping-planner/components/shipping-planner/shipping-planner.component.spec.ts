import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ShippingPlannerComponent } from './shipping-planner.component';
import { RouterTestingModule } from '@angular/router/testing';
import { SearchBarModule } from '../search-bar/search-bar.module';
import { PortListModule } from './port-list/port-list.module';
import { PortMapModule } from './port-map/port-map.module';
import { PortDetailsModule } from './port-details/port-details.module';
import { StoreModule } from '@ngrx/store';
import { SHIPPING_PLANNER_STORE_KEY, ShippingPlannerReducer } from '../../store/shipping-planner.reducer';
import { EffectsModule } from '@ngrx/effects';
import { ShippingPlannerEffects } from '../../store/shipping-planner.effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { reducers, metaReducers } from '../../../../reducers';
import { HttpClientModule } from '@angular/common/http';

describe('ShippingPlannerComponent', () => {
  let component: ShippingPlannerComponent;
  let fixture: ComponentFixture<ShippingPlannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ RouterTestingModule,
        HttpClientModule,
        SearchBarModule,
        PortListModule,
        PortMapModule,
        PortDetailsModule,
        // ngrx store initialization
        StoreRouterConnectingModule.forRoot({ stateKey: 'router' }),
        StoreModule.forRoot(reducers, { metaReducers }),
        EffectsModule.forRoot([]),

        StoreModule.forFeature(SHIPPING_PLANNER_STORE_KEY, ShippingPlannerReducer),
        EffectsModule.forFeature([ShippingPlannerEffects])
      ],
      declarations: [ ShippingPlannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShippingPlannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
