// angular
import {
  Component,
  AfterViewInit,
  ElementRef,
  ViewChild,
  OnDestroy,
  NgZone,
  ComponentFactoryResolver,
  Injector,
  ApplicationRef,
  ComponentRef,
} from '@angular/core';
// map
import L from 'leaflet';
import { icons } from '../../../models/map-icon';

// rxjs
import { Subject, Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

// app imports
import { Port } from '../../../models/port';
import { ShippingPlannerService } from '../../../store/shipping-planner.service';
import { MapInfoWindowComponent } from './map-info-window/map-info-window.component';
import { LocalStorageService } from '../../../../../services/local-storage.service';


@Component({
  selector: 'app-port-map',
  templateUrl: './port-map.component.html',
  styleUrls: ['./port-map.component.scss']
})
export class PortMapComponent implements AfterViewInit, OnDestroy {
  private unsubscribe$: Subject<void> = new Subject<void>();
  // observable properties
  portList$: Observable<Array<Port>>;

  // reference to the map element
  @ViewChild('mapRef') mapRef: ElementRef;
  // marker info popup window ref that will be created dinamicaly when open the popup
  componentMapInfoWindowRef: ComponentRef<MapInfoWindowComponent>;

  _map: any;
  _markerLayers: Map<string, any> = new Map();
  _mapDefaultLat = 28.913943;
  _mapDefaultLon = -94.131125;
  _markers: Array<any> = [];

  constructor(
    private shippingPlannerService: ShippingPlannerService,
    private localStorageService: LocalStorageService,
    private injector: Injector,
    private appRef: ApplicationRef,
    private componentFactoryResolver: ComponentFactoryResolver,
    private zone: NgZone
  ) {
    this.portList$ = this.shippingPlannerService.portList$;

    this.shippingPlannerService.selectedPort$
      .pipe(
        takeUntil(this.unsubscribe$)
      )
      .subscribe(port => {
        // open marker popup info window
        this.openMapMarkerPopupInfo(port);
      });
  }

  ngAfterViewInit() {
    if (!this._map) {
      this.initMap();
    }
    this.loadLayerData(this._map.getBounds());

    // subscribe to the filters observable to show hide marker groups when
    // the switches (isPort, isCruise) change value
    this.shippingPlannerService.filters$
      .pipe(
        takeUntil(this.unsubscribe$)
      )
      .subscribe(filters => {
        if (!filters) {
          return;
        }

        // this can be better,
        // but due to the limited time I had (also I need to study the leaflet map api)
        // I did it the quick way
        filters.isCargo
          ? this.addMapLayer('port')
          : this.removeMapLayer('port');

        filters.isCruise
          ? this.addMapLayer('cruise')
          : this.removeMapLayer('cruise');
      });

    this.portList$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(harbors => {
        this.renderPortsOnMap(harbors);
      });
  }

  // removes a marker layer group by name
  // I need to study more the map api to see if this can be done in a better way
  removeMapLayer(layerName: string) {
    let layerToRemove;
    this._map.eachLayer(layer => {
      if (layer.name === layerName) {
        layerToRemove = layer;
        return;
      }
    });
    if (layerToRemove) {
      this._map.removeLayer(layerToRemove);
    }
  }

  // adds a marker layer group to the map if it is not already on the map
  // I need to study more the map api to see if this can be done in a better way
  addMapLayer(layerName: string) {
    let hasLayer = false;
    this._map.eachLayer(layer => {
      if (layer.name === layerName) {
        hasLayer = true;
        return;
      }
    });
    // test not to add the layer again
    if (!hasLayer) {
      this._map.addLayer(this._markerLayers.get(layerName));
    }
  }

  initMap() {
    // Initialize the Leaflet map
    if (!this._map) {
      const mapBounds = this.getBoundsFromLocalStorage();
      const START_LATLNG = [(mapBounds.maxlat + mapBounds.minlat) / 2, (mapBounds.maxlon + mapBounds.minlon) / 2];
      const START_ZOOM = 7;
      const element = this.mapRef.nativeElement;

      this._map = L.map(element, { trackResize: true }).setView(
        START_LATLNG,
        START_ZOOM
      );

      // Add the basemap tile layer
      L.tileLayer(
        'https://api.mapbox.com/styles/v1/{id}/{z}/{x}/{y}?access_token={accessToken}',
        {
          attribution:
            'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
            '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>' +
            ', Imagery © <a href="http://mapbox.com">Mapbox</a>',
          maxZoom: 18,
          id: 'mapbox/satellite-streets-v10/tiles/256',
          accessToken:
            'pk.eyJ1IjoiYnJhbmRvbmRldiIsImEiOiJjajFwNjNmODAwMDBnMzFwbDJ4N21yZmFmIn0.YC44JxjiM36-I54e-hVQUA'
        }
      ).addTo(this._map);

      // create marker layer groups
      // and add them to map
      icons.forEach((value, key) => {
        const layer = new L.LayerGroup();
        layer.name = key;
        this._markerLayers.set(key, layer);
        this._map.addLayer(layer);
      });

      // Whenever the user pans, load data for the new bounds
      this._map.on('moveend', () => this.loadLayerData(this._map.getBounds()));
    }
  }

  loadLayerData(bounds) {
    this.saveBoundsToLocalStorage(bounds);
    this.shippingPlannerService.getPorts({
      minlat: bounds._southWest.lat,
      minlon: bounds._southWest.lng,
      maxlat: bounds._northEast.lat,
      maxlon: bounds._northEast.lng
    });
  }

  saveBoundsToLocalStorage(bounds: any) {
    this.localStorageService.setItem('mapBounds', {
      maxlat: bounds._northEast.lat,
      minlat: bounds._southWest.lat,
      maxlon: bounds._southWest.lng,
      minlon: bounds._northEast.lng
    });
  }

  getBoundsFromLocalStorage() {
    const bounds = this.localStorageService.getItem('mapBounds');
    if (bounds) {
      return bounds;
    } else {
      return {
        maxlat: this._mapDefaultLat,
        minlat: this._mapDefaultLat,
        maxlon: this._mapDefaultLon,
        minlon: this._mapDefaultLon
      };
    }
  }
  // creates the marker and attaches the click event handler
  // also it creates dinamicaly the popup info window component
  createMarker(port, icon) {
    const layer = L.marker([port.latitude, port.longitude], {
      icon: icon
    });
    layer.id = port.id;
    // onClick create popup window component dinamicaly and append it to the popup
    const popupInfoWindow = L.popup();
    layer.on({
      click: () => {
        // because click listener is running outside angular2 zone
        // we need to explicity run the code inside angular2 zone
        this.zone.run(() => {
          const div = this.renderMapMarkerPopupInfo(port);
          popupInfoWindow.setContent(div);
        });
      }
    });
    layer.bindPopup(popupInfoWindow);
    this._markers.push(layer);
    return layer;
  }

  renderMapMarkerPopupInfo(port: Port) {
    if (this.componentMapInfoWindowRef) {
      this.componentMapInfoWindowRef.destroy();
    }
    // create the instance and pass the 'port' object as input parameter
    const compFactory = this.componentFactoryResolver.resolveComponentFactory(MapInfoWindowComponent);
    this.componentMapInfoWindowRef = compFactory.create(this.injector);
    this.componentMapInfoWindowRef.instance.port = port;

    // necessary for change detection within MapInfoWindowComponent
    if (this.appRef['attachView']) {
      this.appRef['attachView'](this.componentMapInfoWindowRef.hostView);
      this.componentMapInfoWindowRef.onDestroy(() => {
        this.appRef['detachView'](this.componentMapInfoWindowRef.hostView);
      });
    } else {
      this.appRef['registerChangeDetector'](this.componentMapInfoWindowRef.changeDetectorRef);
      this.componentMapInfoWindowRef.onDestroy(() => {
        this.appRef['unregisterChangeDetector'](this.componentMapInfoWindowRef.changeDetectorRef);
      });
    }

    // append content to the map info popup
    const div = document.createElement('div');
    div.appendChild(this.componentMapInfoWindowRef.location.nativeElement);
    return div;
  }

  openMapMarkerPopupInfo(port: Port) {
    if (!port) {
      return;
    }
    const marker = this._markers.find(p => p.id === port.id);
    if (marker) {
      const div = this.renderMapMarkerPopupInfo(port);
      marker._popup.setContent(div);
      marker.openPopup();
    }
  }

  renderPortsOnMap(harbors: Array<Port>) {
    this._markers = [];
    if (!this._markerLayers) {
      return;
    }
    this._markerLayers.forEach((value, key) => value.clearLayers());

    for (const harbor of harbors) {
      this._markerLayers.get(harbor.type).addLayer(this.createMarker(harbor, icons.get(harbor.type)));
    }
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
    // unbind events on map and destroy the map
    if (this._map && this._map.remove) {
      this._map.off();
      this._map.remove();
    }
  }
}
