import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PortMapComponent } from './port-map.component';
import { MapInfoWindowModule } from './map-info-window/map-info-window.module';
import { PortListModule } from '../port-list/port-list.module';

@NgModule({
  imports: [
    CommonModule,
    PortListModule,
    MapInfoWindowModule
  ],
  declarations: [PortMapComponent],
  exports: [PortMapComponent]
})
export class PortMapModule { }
