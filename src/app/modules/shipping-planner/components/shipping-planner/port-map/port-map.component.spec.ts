import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PortMapComponent } from './port-map.component';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from '../../../../../reducers';
import { EffectsModule } from '@ngrx/effects';
import { SHIPPING_PLANNER_STORE_KEY, ShippingPlannerReducer } from '../../../store/shipping-planner.reducer';
import { ShippingPlannerEffects } from '../../../store/shipping-planner.effects';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { PortListComponent } from '../port-list/port-list.component';
import { PortListModule } from '../port-list/port-list.module';

describe('PortMapComponent', () => {
  let component: PortMapComponent;
  let fixture: ComponentFixture<PortMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientModule,
        PortListModule,
        // ngrx store initialization
        EffectsModule.forRoot([]),
        StoreRouterConnectingModule.forRoot({ stateKey: 'router' }),
        StoreModule.forRoot(reducers, { metaReducers }),

        StoreModule.forFeature(SHIPPING_PLANNER_STORE_KEY, ShippingPlannerReducer),
        EffectsModule.forFeature([ShippingPlannerEffects])
      ],
      declarations: [ PortMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
