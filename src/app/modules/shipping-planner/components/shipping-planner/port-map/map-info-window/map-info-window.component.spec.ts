import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MapInfoWindowComponent } from './map-info-window.component';
import { Port } from '../../../../models/port';
import { RouterTestingModule } from '@angular/router/testing';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from '../../../../../../reducers';
import { SHIPPING_PLANNER_STORE_KEY, ShippingPlannerReducer } from '../../../../store/shipping-planner.reducer';
import { ShippingPlannerEffects } from '../../../../store/shipping-planner.effects';
import { HttpClientModule } from '@angular/common/http';

describe('MapInfoWindowComponent', () => {
  let component: MapInfoWindowComponent;
  let fixture: ComponentFixture<MapInfoWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FontAwesomeModule,
        RouterTestingModule,
        HttpClientModule,
        // ngrx store initialization
        EffectsModule.forRoot([]),
        StoreRouterConnectingModule.forRoot({ stateKey: 'router' }),
        StoreModule.forRoot(reducers, { metaReducers }),

        StoreModule.forFeature(SHIPPING_PLANNER_STORE_KEY, ShippingPlannerReducer),
        EffectsModule.forFeature([ShippingPlannerEffects])
      ],
      declarations: [ MapInfoWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapInfoWindowComponent);
    component = fixture.componentInstance;
    component.port = new Port(1, 'Port Name', 1, 1, 'Port City', '', 1, 'cruise');
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
