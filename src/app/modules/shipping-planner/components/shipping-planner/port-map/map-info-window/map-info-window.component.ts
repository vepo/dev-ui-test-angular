import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Port } from '../../../../models/port';
import { ShippingPlannerService } from '../../../../store/shipping-planner.service';

@Component({
  selector: 'app-map-info-window',
  templateUrl: './map-info-window.component.html',
  styleUrls: ['./map-info-window.component.scss']
})
export class MapInfoWindowComponent implements OnInit {
  @Input() port: Port;
  constructor(private router: Router, private shippingPlannerService: ShippingPlannerService) {
  }

  ngOnInit() {
  }
  onSelect(port: Port) {
    // set the current selected port and route to port details page
    this.shippingPlannerService.selectPort(port);
    // route to port details page
    // we could also move this in select port action/effect
    this.router.navigate(['shipping-planner/details', port.id]);
  }
}
