import { MapInfoWindowModule } from './map-info-window.module';

describe('MapInfoWindowModule', () => {
  let mapInfoWindowModule: MapInfoWindowModule;

  beforeEach(() => {
    mapInfoWindowModule = new MapInfoWindowModule();
  });

  it('should create an instance', () => {
    expect(mapInfoWindowModule).toBeTruthy();
  });
});
