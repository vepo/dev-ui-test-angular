// angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
// font awesome icons
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
// app imports
import { MapInfoWindowComponent } from './map-info-window.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FontAwesomeModule
  ],
  declarations: [MapInfoWindowComponent],
  exports: [MapInfoWindowComponent],
  // dynamically render this component at runtime
  entryComponents: [MapInfoWindowComponent]
})
export class MapInfoWindowModule { }
