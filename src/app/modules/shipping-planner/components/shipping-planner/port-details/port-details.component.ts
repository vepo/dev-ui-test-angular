// angular
import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
// rxjs
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
// app imports
import { ApiService } from '../../../../../services/api.service';
import { Port } from '../../../models/port';

@Component({
  selector: 'app-port-details',
  templateUrl: './port-details.component.html',
  styleUrls: ['./port-details.component.scss']
})
export class PortDetailsComponent implements OnDestroy {
  private unsubscribe$: Subject<void> = new Subject<void>();
  port: Port;

  constructor(private route: ActivatedRoute, private apiService: ApiService) {
    this.route.params
      .pipe(
        takeUntil(this.unsubscribe$)
      )
      .subscribe(params => {
        this.apiService.getPortDetails(params['id'])
          .pipe(takeUntil(this.unsubscribe$))
          .subscribe(
            port => this.port = port
          );
      });
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
