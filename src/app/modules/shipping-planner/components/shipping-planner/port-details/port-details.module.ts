import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// app imports
import { PortDetailsComponent } from './port-details.component';
import { MapModule } from './map/map.module';
import { AppCommonModule } from '../../../../../app.common.module';

// font awesome icons
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { PortListModule } from '../port-list/port-list.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FontAwesomeModule,
    AppCommonModule,
    MapModule,
    PortListModule
  ],
  declarations: [PortDetailsComponent],
  exports: [PortDetailsComponent]
})
export class PortDetailsModule { }
