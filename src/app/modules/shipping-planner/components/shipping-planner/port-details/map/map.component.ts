// angular
import { Component, ViewChild, ElementRef, AfterViewInit, Input, OnDestroy, OnChanges } from '@angular/core';
// map
import L from 'leaflet';
import { icons } from '../../../../models/map-icon';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements AfterViewInit, OnDestroy, OnChanges {
  @ViewChild('mapRef') mapRef: ElementRef;
  @Input() latitude: number;
  @Input() longitude: number;
  @Input() type: string;

  _map: any;
  _layer: any;
  START_ZOOM = 7;

  constructor() {}

  ngAfterViewInit() {
    this.initMap();
  }

  ngOnChanges() {
    // refresh marker on the map
    if (this._layer) {
      this._layer.clearLayers();
      this._layer.addLayer(this.createMarker(this.latitude, this.longitude, icons.get(this.type)));
      this._map.setView(
        [this.latitude, this.longitude], this.START_ZOOM
      );
    }
  }

  initMap() {
    const element = this.mapRef.nativeElement;

    // Initialize the Leaflet map
    this._map = L.map(element, { zoomControl: false, trackResize: true }).setView(
      [this.latitude, this.longitude],
      this.START_ZOOM
    );


    // Add the basemap tile layer
    L.tileLayer(
      'https://api.mapbox.com/styles/v1/{id}/{z}/{x}/{y}?access_token={accessToken}',
      {
        attribution:
          'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
          '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>' +
          ', Imagery © <a href="http://mapbox.com">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/satellite-streets-v10/tiles/256',
        accessToken:
          'pk.eyJ1IjoiYnJhbmRvbmRldiIsImEiOiJjajFwNjNmODAwMDBnMzFwbDJ4N21yZmFmIn0.YC44JxjiM36-I54e-hVQUA'
      }
    ).addTo(this._map);

    // add marker
    this._layer = new L.LayerGroup();
    this._map.addLayer(this._layer);
    this._layer.addLayer(this.createMarker(this.latitude, this.longitude, icons.get(this.type)));
  }

  createMarker(latitude, longitude, icon) {
    return L.marker([latitude, longitude], {
      icon: icon
    });
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    if (this._map && this._map.remove) {
      this._map.off();
      this._map.remove();
    }
  }
}
