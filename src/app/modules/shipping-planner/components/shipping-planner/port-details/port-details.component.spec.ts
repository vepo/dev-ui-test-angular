import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PortDetailsComponent } from './port-details.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MapModule } from './map/map.module';
import { HttpClientModule } from '@angular/common/http';
import { AppCommonModule } from '../../../../../app.common.module';
import { Port } from '../../../models/port';
import { PortListModule } from '../port-list/port-list.module';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreModule } from '@ngrx/store';
import { metaReducers, reducers } from '../../../../../reducers';
import { SHIPPING_PLANNER_STORE_KEY, ShippingPlannerReducer } from '../../../store/shipping-planner.reducer';
import { ShippingPlannerEffects } from '../../../store/shipping-planner.effects';

describe('PortDetailsComponent', () => {
  let component: PortDetailsComponent;
  let fixture: ComponentFixture<PortDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule,
        AppCommonModule, PortListModule, FontAwesomeModule, MapModule, HttpClientModule,
        // ngrx store initialization
        EffectsModule.forRoot([]),
        StoreRouterConnectingModule.forRoot({ stateKey: 'router' }),
        StoreModule.forRoot(reducers, { metaReducers }),

        StoreModule.forFeature(SHIPPING_PLANNER_STORE_KEY, ShippingPlannerReducer),
        EffectsModule.forFeature([ShippingPlannerEffects]),
      ],
      declarations: [PortDetailsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
