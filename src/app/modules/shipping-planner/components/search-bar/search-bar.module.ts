import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// font awesome icons
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
// app imports
import { SearchBarComponent } from './search-bar.component';

@NgModule({
  imports: [
    CommonModule,
    FontAwesomeModule
  ],
  declarations: [SearchBarComponent],
  exports: [SearchBarComponent]
})
export class SearchBarModule { }
