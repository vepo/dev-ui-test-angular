// angular imports
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// app imports
import { PortMapComponent } from './components/shipping-planner/port-map/port-map.component';
import { ShippingPlannerComponent } from './components/shipping-planner/shipping-planner.component';
import { PortDetailsComponent } from './components/shipping-planner/port-details/port-details.component';

const routes: Routes = [
  {
    path: '',
    component: ShippingPlannerComponent,
    children: [
      {
        path: '',
        component: PortMapComponent,
        data: { title: 'Map' }
      },
      {
        path: 'map',
        component: PortMapComponent,
        data: { title: 'Map' }
      },
      {
        path: 'details/:id',
        component: PortDetailsComponent,
        data: { title: 'Details' },
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShippingPlannerRoutingModule {}
