// angular imports
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// ngrx store
import { StoreModule } from '@ngrx/store';
import { SHIPPING_PLANNER_STORE_KEY, ShippingPlannerReducer } from './store/shipping-planner.reducer';
import { EffectsModule } from '@ngrx/effects';
import { ShippingPlannerEffects } from './store/shipping-planner.effects';

// app imports
import { ShippingPlannerComponent } from './components/shipping-planner/shipping-planner.component';
import { ShippingPlannerRoutingModule } from './shipping-planner-routing.module';
import { SearchBarModule } from './components/search-bar/search-bar.module';
import { PortListModule } from './components/shipping-planner/port-list/port-list.module';
import { PortMapModule } from './components/shipping-planner/port-map/port-map.module';
import { PortDetailsModule } from './components/shipping-planner/port-details/port-details.module';
import { AppCommonModule } from '../../app.common.module';

@NgModule({
  imports: [
    CommonModule,
    AppCommonModule,
    ShippingPlannerRoutingModule,
    SearchBarModule,
    PortListModule,
    PortMapModule,
    PortDetailsModule,
    // ngrx store initialization
    StoreModule.forFeature(SHIPPING_PLANNER_STORE_KEY, ShippingPlannerReducer),
    EffectsModule.forFeature([ShippingPlannerEffects]),
  ],
  declarations: [ShippingPlannerComponent],
  exports: [ShippingPlannerComponent]
})
export class ShippingPlannerModule { }
