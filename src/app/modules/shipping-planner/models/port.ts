export class Port {
    id: number;
    name: string;
    latitude: number;
    longitude: number;
    city: string;
    website: string;
    natlScale: number;
    type: string;
    constructor(id: number, name: string, latitude: number,
        longitude: number, city: string, website: string,
        natlScale: number, type: string) {

        this.id = id;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.city = city;
        this.website = website;
        this.natlScale = natlScale;
        this.type = type;
    }
}
