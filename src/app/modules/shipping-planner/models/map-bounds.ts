export interface MapBounds {
    minlat: number;
    minlon: number;
    maxlat: number;
    maxlon: number;
}
