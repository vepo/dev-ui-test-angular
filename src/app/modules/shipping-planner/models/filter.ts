export class Filter {
    isCruise: boolean;
    isCargo: boolean;
    term?: string;

    constructor(isCruise: boolean, isCargo: boolean, term?: string) {
        this.isCruise = isCruise;
        this.isCargo = isCargo;
        this.term = term;
    }
}
