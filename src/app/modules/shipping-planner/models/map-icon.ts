// map
import L from 'leaflet';

// define the set of icons used in map pins
export const icons = new Map([
  ['cruise', L.icon({
    iconUrl: '/assets/icons/cruise.png',
    iconSize: [28, 45],
    iconAnchor: [28, 45],
    popupAnchor: [-14, -45]
  })],
  ['port', L.icon({
    iconUrl: '/assets/icons/port.png',
    iconSize: [28, 45],
    iconAnchor: [28, 45],
    popupAnchor: [-14, -45]
  })]
]);
