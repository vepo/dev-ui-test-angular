import { Port } from '../models/port';
import { ShippingPlannerActions, ShippingPlannerActionTypes } from './shipping-planner.actions';
import { Filter } from '../models/filter';

export const SHIPPING_PLANNER_STORE_KEY = 'shipping_planner';

export interface ShippingPlannerState {
    isLoading: boolean;
    portList: Array<Port>;
    selectedPort: Port;
    filters: Filter;
}

export const initialState: ShippingPlannerState = {
    isLoading: false,
    portList: [],
    selectedPort: undefined,
    filters: new Filter(true, true)
};

export function ShippingPlannerReducer(
    state: ShippingPlannerState = initialState,
    action: ShippingPlannerActions
): ShippingPlannerState {
    switch (action.type) {
        case ShippingPlannerActionTypes.LOAD_PORTS:
            return { ...state, isLoading: true };

        case ShippingPlannerActionTypes.LOAD_PORTS_SUCCESS:
            return { ...state, isLoading: false, selectedPort: undefined, portList: [...action.payload] };

        case ShippingPlannerActionTypes.LOAD_PORTS_ERROR:
            return { ...state, ...initialState, isLoading: false };

        case ShippingPlannerActionTypes.SELECT_PORT:
            return { ...state,  selectedPort: action.payload};

        case ShippingPlannerActionTypes.UPDATE_PORT_TYPE_FILTER:
            return {
                ...state,
                filters: {
                    ...state.filters,
                    isCargo: action.payload.isCargo,
                    isCruise: action.payload.isCruise
                }
            };

        default:
            return state;
    }
}
