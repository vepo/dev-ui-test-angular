import { Action } from '@ngrx/store';
import { Filter } from '../models/filter';
import { Port } from '../models/port';
import { MapBounds } from '../models/map-bounds';

// define the actions names
export enum ShippingPlannerActionTypes {
  LOAD_PORTS = '[Shipping Planner] Load Ports',
  LOAD_PORTS_SUCCESS = '[Shipping Planner] Load Ports Success',
  LOAD_PORTS_ERROR = '[Shipping Planner] Load Ports Error',
  SELECT_PORT = '[Shipping Planner] Select Port',
  UPDATE_PORT_TYPE_FILTER = '[Shipping Planner] Update Port Type Filter',
}

// define the actions
export class ActionLoadPorts implements Action {
  readonly type = ShippingPlannerActionTypes.LOAD_PORTS;
  constructor(public payload?: MapBounds) {}
}

export class ActionLoadPortsSuccess implements Action {
  readonly type = ShippingPlannerActionTypes.LOAD_PORTS_SUCCESS;
  constructor(public payload?: Array<Port>) {}
}

export class ActionLoadPortsError implements Action {
  readonly type = ShippingPlannerActionTypes.LOAD_PORTS_ERROR;
  constructor(public payload?: any) {}
}

export class ActionSelectPort implements Action {
  readonly type = ShippingPlannerActionTypes.SELECT_PORT;
  constructor(public payload?: Port) {}
}

export class ActionUpdatePortTypeFilter implements Action {
  readonly type = ShippingPlannerActionTypes.UPDATE_PORT_TYPE_FILTER;
  constructor(public payload?: {isCruise: boolean, isCargo: boolean}) {}
}

// export actions
export type ShippingPlannerActions
                = ActionLoadPorts
                | ActionLoadPortsSuccess
                | ActionLoadPortsError
                | ActionSelectPort
                | ActionUpdatePortTypeFilter;
