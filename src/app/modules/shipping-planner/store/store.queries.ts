import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ShippingPlannerState, SHIPPING_PLANNER_STORE_KEY } from './shipping-planner.reducer';

export const getShippingPlannerState = createFeatureSelector<ShippingPlannerState>(
    SHIPPING_PLANNER_STORE_KEY
);

// store queries
export const getPortList = createSelector(
    getShippingPlannerState,
    (state: ShippingPlannerState) => state.portList
);

export const getSelectedPort = createSelector(
    getShippingPlannerState,
    (state: ShippingPlannerState) => state.selectedPort
);

export const getLoadingState = createSelector(
    getShippingPlannerState,
    (state: ShippingPlannerState) => state.isLoading
);

export const getFilters = createSelector(
    getShippingPlannerState,
    (state: ShippingPlannerState) => state.filters
);
