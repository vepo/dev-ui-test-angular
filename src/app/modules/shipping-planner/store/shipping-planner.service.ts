// angular
import { Injectable } from '@angular/core';
// ngrx store
import { Store, select } from '@ngrx/store';
import { ShippingPlannerState } from './shipping-planner.reducer';
import { getPortList, getSelectedPort, getFilters } from './store.queries';
import * as shippingPlannerActions from './shipping-planner.actions';
// models
import { Port } from '../models/port';
import { MapBounds } from '../models/map-bounds';
import { Filter } from '../models/filter';


@Injectable({
    providedIn: 'root'
})
export class ShippingPlannerService {
    // Observable Queries available for consumption by views
    portList$ = this.store.pipe(select(getPortList));
    selectedPort$ = this.store.pipe(select(getSelectedPort));
    filters$ = this.store.pipe(select(getFilters));

    constructor(
        private store: Store<ShippingPlannerState>,
    ) { }


    getPorts(bounds: MapBounds) {
        this.store.dispatch(new shippingPlannerActions.ActionLoadPorts(bounds));
    }
    selectPort(port: Port) {
        this.store.dispatch(new shippingPlannerActions.ActionSelectPort(port));
    }
    updatePortTypeFilter(portFilterType: Filter) {
        this.store.dispatch(new shippingPlannerActions.ActionUpdatePortTypeFilter(portFilterType));
    }
}
