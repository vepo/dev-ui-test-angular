// angular
import { Injectable } from '@angular/core';

// ngrx store
import { Action } from '@ngrx/store';
import { Effect, Actions, ofType } from '@ngrx/effects';
import {
    ShippingPlannerActionTypes, ActionLoadPorts,
    ActionLoadPortsSuccess, ActionLoadPortsError
} from './shipping-planner.actions';

// rxjs
import { Observable, of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';

// app imports
import { ApiService } from '../../../services/api.service';

@Injectable({
    providedIn: 'root'
})
export class ShippingPlannerEffects {
    // get port list effect
    @Effect() getports$: Observable<Action> = this.actions$
        .pipe(ofType(ShippingPlannerActionTypes.LOAD_PORTS),
            map((action: ActionLoadPorts) => action.payload),
            switchMap(payload => {
                return this.apiService.getHarbors(payload)
                    .pipe(
                        map(data => {
                            return new ActionLoadPortsSuccess(data);
                        }),
                        catchError(err => { console.log(err); return of(new ActionLoadPortsError()); })
                    );
            }),
        );


    constructor(
        private actions$: Actions<Action>, private apiService: ApiService
    ) { }
}
